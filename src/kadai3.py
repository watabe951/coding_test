import argparse
from itertools import count
import sys

import datetime
from typing import Optional
from logrecord import LogRecord
from failure import Failure, AveragePingTooHigh
from ipaddress import IPv4Interface

from utils import read_files, read_stdin


def is_recent_avarage_ping_over(records: list[LogRecord], i:int,  m: int, t: float)-> bool:
    if i < 0 or i >= len(records):
        raise Exception("i must be satisfy: 0 <= i and i < len(records) ")
    if m <= 0:
        raise Exception("n must be non-zero positive integer")

    sum = 0.0
    counter = 0
    for d in range(m):
        if i - d >= 0:
            sum += records[i - d].result.ping()
            counter += 1
    average = sum / counter
    return average > t
    
def get_failures(records: list[LogRecord], m: int, t: float) -> list[AveragePingTooHigh]:
    if m <= 0:
        raise Exception("n must be non-zero positive integer")

    failures: list[AveragePingTooHigh] = []

    is_in_failure: bool = False
    failure_started_at: Optional[datetime.datetime]  = None
    failure_ended_at: Optional[datetime.datetime] = None

    for i, record in enumerate(records):
        if is_recent_avarage_ping_over(records, i, m, t):
            if not is_in_failure:
                failure_started_at = record.time
                failure_ended_at = None
            else:
                failure_ended_at = record.time
            is_in_failure = True
        else:
            if not is_in_failure:
                failure_started_at = None
                failure_ended_at = None
            else:
                failure_ended_at = record.time
                if failure_started_at is not None:
                    failures.append(AveragePingTooHigh(failure_started_at, failure_ended_at, record.ip_address))
                else:
                    raise Exception("failure_started_at is None when failure is ended")
            is_in_failure = False
    else:
        if is_in_failure:
            if failure_started_at is not None:
                failures.append(AveragePingTooHigh(failure_started_at, None, record.ip_address))
            else:
                raise Exception("failure_started_at is None when failure is ended")
    return failures



def detect_average_ping_too_high(path: Optional[str], m: int, t: float):
    lines : list[str]
    if path is not None:
        lines = read_files(path)
    else:
        lines = read_stdin()

    dic: dict[IPv4Interface, list[LogRecord]] = {}
    for line in lines:
        time, ip, res = line.split(",")
        record: LogRecord = LogRecord.create(time, ip, res)
        try:
            dic[record.ip_address].append(record)
        except KeyError:
            dic[record.ip_address] = [record]

    fault_ip_set = set()
    for k, records in dic.items():
        for record in records:
            if record.result.is_timeout():
                fault_ip_set.add(record.ip_address)
    
    for k, records in dic.items():
        failures = get_failures(records, m, t)
        for f in failures:
            print(f)
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, default=None)
    parser.add_argument("-m", type=int, default=1)
    parser.add_argument("-t", type=int, default=1)
    args = parser.parse_args()

    detect_average_ping_too_high(args.input, args.m, args.t)

if __name__ == "__main__":
    main()