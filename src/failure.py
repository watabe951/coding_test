import datetime
import ipaddress

from typing import Optional

class Failure():
    def __init__(self, 
        start_at: datetime.datetime,
        end_at: Optional[datetime.datetime],
        ip_addr: ipaddress.IPv4Interface
        ) -> None:
        self.start_at: datetime.datetime = start_at
        self.end_at: Optional[datetime.datetime] = end_at
        self.ip_addr: ipaddress.IPv4Interface = ip_addr
    def __str__(self) -> str:
        if self.end_at is not None:
            return f"Failure. IP: {self.ip_addr}, From: {self.start_at}, To: {self.end_at}"
        else:
            return f"Failure. IP: {self.ip_addr}, From: {self.start_at}, To: Unknown"

class NetworkFailure():
    def __init__(self, 
        start_at: datetime.datetime,
        end_at: Optional[datetime.datetime],
        ip_network: ipaddress.IPv4Network
        ) -> None:
        self.start_at = start_at
        self.end_at = end_at
        self.ip_network = ip_network
    def __str__(self) -> str:
        if self.end_at is not None:
            return f"NetworkFailure. Network: {self.ip_network}, From: {self.start_at}, To: {self.end_at}"
        else:
            return f"NetworkFailure. Network: {self.ip_network}, From: {self.start_at}, To: Unknown"


class AveragePingTooHigh():
    def __init__(self, 
        start_at: datetime.datetime,
        end_at: Optional[datetime.datetime],
        ip_addr: ipaddress.IPv4Interface,
        ) -> None:
        self.start_at: datetime.datetime = start_at
        self.end_at: Optional[datetime.datetime] = end_at
        self.ip_addr: ipaddress.IPv4Interface = ip_addr
    def __str__(self) -> str:
        if self.end_at is not None:
            return f"Average ping too high. IP: {self.ip_addr}, From: {self.start_at}, To: {self.end_at}"
        else:
            return f"Average ping too high. IP: {self.ip_addr}, From: {self.start_at}, To: Unknown"

ServerFailureLogs = dict[ipaddress.IPv4Interface, list[Failure]]
ServerFailureLogsForeachNetwork = dict[ipaddress.IPv4Network, ServerFailureLogs]

