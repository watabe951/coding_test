import argparse
import sys

import datetime
from typing import Optional
from logrecord import LogRecord
from failure import Failure
from ipaddress import IPv4Interface

from utils import read_files, read_stdin

def get_failures(records: list[LogRecord]) -> list[Failure]:
    failures: list[Failure] = []

    is_in_failure: bool = False
    failure_started_at: Optional[datetime.datetime]  = None
    failure_ended_at: Optional[datetime.datetime] = None

    for record in records:
        if record.result.is_timeout():
            if not is_in_failure:
                failure_started_at = record.time
                failure_ended_at = None
            else:
                failure_ended_at = record.time
            is_in_failure = True
        else:
            if not is_in_failure:
                failure_started_at = None
                failure_ended_at = None
            else:
                failure_ended_at = record.time
                if failure_started_at is not None:
                        failures.append(Failure(failure_started_at, failure_ended_at, record.ip_address))
                else:
                    raise Exception("failure_started_at is None when failure is ended")
            is_in_failure = False
    else:
        if is_in_failure:
            if failure_started_at is not None:
                failures.append(Failure(failure_started_at, None, record.ip_address))
            else:
                raise Exception("failure_started_at is None when failure is ended")
    return failures



def detect_failure(path: Optional[str]):
    lines : list[str]
    if path is not None:
        lines = read_files(path)
    else:
        lines = read_stdin()


    dic: dict[IPv4Interface, list[LogRecord]] = {}
    for line in lines:
        time, ip, res = line.split(",")
        record: LogRecord = LogRecord.create(time, ip, res)
        try:
            dic[record.ip_address].append(record)
        except KeyError:
            dic[record.ip_address] = [record]

    fault_ip_set = set()
    for k, records in dic.items():
        for record in records:
            if record.result.is_timeout():
                fault_ip_set.add(record.ip_address)
    
    for k, records in dic.items():
        failures = get_failures(records)
        for f in failures:
            print(f)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, default=None)


    args = parser.parse_args()

    detect_failure(args.input)

if __name__ == "__main__":
    main()