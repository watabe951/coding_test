import argparse

import datetime
from typing import Optional

from itertools import product
from logrecord import LogRecord
from failure import Failure, NetworkFailure, ServerFailureLogs, ServerFailureLogsForeachNetwork
from ipaddress import IPv4Interface, IPv4Network
from collections import defaultdict

from utils import read_files, read_stdin


def create_network_failure_logs(failure_dict: ServerFailureLogs) -> ServerFailureLogsForeachNetwork:
    ddic: defaultdict[IPv4Network, ServerFailureLogs] = defaultdict(lambda: defaultdict(list))
    for k, v in failure_dict.items():
        ddic[k.network][k] = v
    dic: ServerFailureLogsForeachNetwork = dict(ddic)
    for k_ in dic.keys():
        dic[k_] = dict(dic[k_])
    return dic

def get_overlap(failures: list[Failure]) -> Optional[tuple[datetime.datetime, Optional[datetime.datetime]]]:
    starts = [x.start_at for x in failures]
    ends = [x.end_at for x in failures if x.end_at is not None]
    
    latest_start = max(starts)
    earliest_end = min(ends) if len(ends) > 0 else None
    if earliest_end is None:
        return (latest_start, earliest_end)
    else:
        if latest_start < earliest_end:
            return (latest_start, earliest_end)
        else:
            return None

def get_overlaps_from_dict(failures_dict: ServerFailureLogs) -> Optional[list[NetworkFailure]]:
    ret = []
    for x in list(product(*failures_dict.values())):
        overlap = get_overlap(list(x))
        if overlap is not None:
            start, end = overlap
            ret.append(NetworkFailure(start, end, x[0].ip_addr.network))
    
    if len(ret) == 0:
        return None
    else:
        return ret

def get_all_overlap(failure_dict: ServerFailureLogs) -> Optional[list[NetworkFailure]]:
    network_dict : ServerFailureLogsForeachNetwork
    network_dict = create_network_failure_logs(failure_dict)
    
    ret : list[NetworkFailure] = []
    for k, v in network_dict.items():
        if k.num_addresses != len(v):
            continue

        l = get_overlaps_from_dict(v)
        if l is not None:
            ret += l
    if len(ret) == 0:
        return None
    else:
        return ret


def get_failures(records: list[LogRecord], n: int) -> list[Failure]:
    if n == 0:
        raise Exception("n must be non-zero positive integer")

    failures: list[Failure] = []

    failure_count: int = 0
    failure_started_at: Optional[datetime.datetime]  = None
    failure_ended_at: Optional[datetime.datetime] = None

    for record in records:
        if record.result.is_timeout():
            if failure_count == 0:
                failure_started_at = record.time
                failure_ended_at = None
            else:
                failure_ended_at = record.time
            failure_count += 1
        else:
            if failure_count == 0:
                failure_started_at = None
                failure_ended_at = None
            else:
                failure_ended_at = record.time
                if failure_count >= n:
                    if failure_started_at is not None:
                        failures.append(Failure(failure_started_at, failure_ended_at, record.ip_address))
                    else:
                        raise Exception("failure_started_at is None when failure is ended")
            failure_count = 0
    else:
        if failure_count > 0:
            if failure_count >= n:
                if failure_started_at is not None:
                        failures.append(Failure(failure_started_at, None, record.ip_address))
                else:
                    raise Exception("failure_started_at is None when failure is ended")
    return failures



def detect_network_failure(path: Optional[str], n: int):
    lines : list[str]
    if path is not None:
        lines = read_files(path)
    else:
        lines = read_stdin()

    dic: defaultdict[IPv4Interface, list[LogRecord]] = defaultdict(list)
    for line in lines:
        time, ip, res = line.split(",")
        record: LogRecord = LogRecord.create(time, ip, res)
        
        dic[record.ip_address].append(record)

    fault_ip_set = set()
    for k, records in dic.items():
        for record in records:
            if record.result.is_timeout():
                fault_ip_set.add(record.ip_address)
    
    failure_dict: dict[IPv4Interface, list[Failure]] = dict()
    for k, records in dic.items():
        failures = get_failures(records, n)
        failure_dict[k] = failures
    
    network_failures: Optional[list[NetworkFailure]] = get_all_overlap(failure_dict)
    if network_failures is not None:
        for f in network_failures:
            print(f)
    else:
        print("No network failure is detected")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, default=None)
    parser.add_argument("-n", type=int, default=1)


    args = parser.parse_args()

    detect_network_failure(args.input, args.n)


if __name__ == "__main__":
    main()