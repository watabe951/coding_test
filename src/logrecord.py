import datetime
import ipaddress
from math import inf


class PingResult(object):
    def __init__(self, s:str) -> None:
        self.result = s
    def __str__(self) -> str:
        return self.result
    def ping(self) -> float:
        if self.result == "-":
            return inf
        else:
            return float(self.result)
    def is_timeout(self):
        if self.result == "-":
            return True
        else:
            return False

class LogRecord(object):
    def __init__(
        self,
        time: datetime.datetime,
        ip_address: ipaddress.IPv4Interface,
        result: PingResult
        ):
        self.time = time
        self.ip_address = ip_address
        self.result = result
    
    def __str__(self) -> str:
        return f"datetime: {self.time}, IP: {self.ip_address}, result: {self.result}"

    @staticmethod
    def create(time: str, ip_address: str, result: str):
        time_ = datetime.datetime.strptime(time, r"%Y%m%d%H%M%S")
        ip_address_ = ipaddress.ip_interface(ip_address)
        result_ = PingResult(result)
        return LogRecord(time_, ip_address_, result_)