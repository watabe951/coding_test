import sys
import argparse

import datetime
from typing import Optional
from logrecord import LogRecord
from failure import Failure
from ipaddress import IPv4Interface
from utils import read_stdin, read_files

def get_failures(records: list[LogRecord], n: int) -> list[Failure]:
    if n == 0:
        raise Exception("n must be non-zero positive integer")

    failures: list[Failure] = []

    failure_count: int = 0
    failure_started_at: Optional[datetime.datetime]  = None
    failure_ended_at: Optional[datetime.datetime] = None

    for record in records:
        if record.result.is_timeout():
            if failure_count == 0:
                failure_started_at = record.time
                failure_ended_at = None
            else:
                failure_ended_at = record.time
            failure_count += 1
        else:
            if failure_count == 0:
                failure_started_at = None
                failure_ended_at = None
            else:
                failure_ended_at = record.time
                if failure_count >= n:
                    if failure_started_at is not None:
                        failures.append(Failure(failure_started_at, failure_ended_at, record.ip_address))
                    else:
                        raise Exception("failure_started_at is None when failure is ended")
            failure_count = 0
    else:
        if failure_count > 0:
            if failure_count >= n:
                if failure_started_at is not None:
                        failures.append(Failure(failure_started_at, None, record.ip_address))
                else:
                    raise Exception("failure_started_at is None when failure is ended")
    return failures


def detect_failure_n(path: Optional[str], n: int):
    lines : list[str]
    if path is not None:
        lines = read_files(path)
    else:
        lines = read_stdin()

    dic: dict[IPv4Interface, list[LogRecord]] = {}
    for line in lines:
        time, ip, res = line.split(",")
        record: LogRecord = LogRecord.create(time, ip, res)
        try:
            dic[record.ip_address].append(record)
        except KeyError:
            dic[record.ip_address] = [record]

    fault_ip_set = set()
    for k, records in dic.items():
        for record in records:
            if record.result.is_timeout():
                fault_ip_set.add(record.ip_address)
    
    for k, records in dic.items():
        failures = get_failures(records, n)
        for f in failures:
            print(f)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, default=None)
    parser.add_argument("-n", type=int, default=1)


    args = parser.parse_args()

    detect_failure_n(args.input, args.n)

if __name__ == "__main__":
    main()