import sys

def read_stdin() -> list[str]:
    lines = []
    while line := sys.stdin.readline():
        line = line.strip("\n")
        lines.append(line)
    return lines

def read_files(path: str) -> list[str]:
    lines = []
    with open(path, "r") as f:
        while line := f.readline():
            line = line.strip("\n")
            lines.append(line)
    return lines