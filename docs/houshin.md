# 回答方針

## 設問1の回答方針
初めにログデータを、`dict[IPv4Interface, list[LogRecord]]`の形にパースする。
パース後のログをもとに、書くサーバの故障期間を出力する。
複数の故障期間がある場合は、すべて出力する。

故障期間は`list[LogRecord]`を先頭から読み取って計算する。疑似コードは以下の通り。

```Python
record_list : list[LogRecord]
failures = []
is_failure = False
for record in record_list:
    if record.is_timeout():
        if not is_failure:
            started_at = record.time
            ended_at = None
        else:
            ended_at = record.time
        is_failure = True
    else:
        if not is_failure:
            started_at = None
            ended_at = None
        else:
            ended_at = record.time
            failures.append(Failure(start_at, ended_at, ip))
        is_failure = True

# 終了時に故障中だったら、それも登録する。
if is_failure:
    failures.append(Failure(start_at, None, ip))

return failure
```


## 設問2の回答方針

設問１の故障期間を計算する関数を改変する。
複数回タイムアウトが続かないと`is_failure`がTrueにならないようにする。

## 設問3の回答方針

`list[LogRecord]`を先頭から読み取り、各レコ―ドに対して直近の平均pingを計算する。
直近の平均pingが閾値以上だったら故障判定とする。

## 設問4の回答方針

あらかじめ、各サーバーが故障している期間を計算しておく。
つぎに、サブネットごとに、そこに属するすべてのサーバが一回以上故障しているかどうかを判定する。「故障したことのあるサーバ数==サブネットに属するサーバ数」で判定。

全てのサーバが故障したことのあるサブネットに対して、
サブネット内の全てのサーバのタイムアウト期間が重なる範囲を探す。
サブネットのサーバが３つの場合の疑似コードは以下のとおり。

```Python
overlap = []
timeout1, timeout2, timeout3: list # 各サーバの故障期間のリスト [(start_at, end_at),...]
for x1 in timeout1:
    for x2 in timeout2:
        for x3 in timeout3:
            start_at, end_at = get_overlap(x1, x2, x3)
            overlap.append([start_at, end_at])
return overlap
```

オーバラップしている期間は、各故障期間の開始時間の一番遅い時間から、一番早い終了時間までとして
計算できる。前者より後者が早い場合は、あるサーバの故障があるサーバの故障開始より先に終わっているので、
全てが重なる故障期間はない。
サブネットのサーバが３つの場合の疑似コードは以下のとおり。

```Python
def get_overlap(x1, x2, x3):
    latest_start_at = max([x1.start_at, x2.start_at, x3.start_at])
    earliest_end_at = min([x1.end_at, x2.end_at, x3.end_at])
    if latest_start_at < earliest_end_at:
        return (latest_start_at, earliest_end_at)
    else:
        return None
```
